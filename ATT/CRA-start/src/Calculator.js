import React from "react";
import "./Calculator.css";
import mobilityPlans from "./services/MobilityPlanService.js";
import Blade from "./components/Blade";
import DeviceOptionList from "./components/DeviceOptionList";
import DataFeature from "./components/features/DataFeature";
import StreamingFeature from "./components/features/StreamingFeature";
import HotspotFeature from "./components/features/HotspotFeature";
import PremiumEntertainmentFeature from "./components/features/PremiumEntertainmentFeature";

class Calculator extends React.Component {
  constructor(props) {
    super(props);
    const maximumDevices = parseInt(this.props.maximumDevices) || 0;
    const selectedDevices = parseInt(this.props.minimumDevices) || 0;
    const availableDevices = maximumDevices - selectedDevices;
    this.state = {
      enabledSmartphones: maximumDevices,
      selectedSmartphones: selectedDevices,
      enabledTablets: availableDevices,
      selectedTablets: 0,
      enabledWearables: availableDevices,
      selectedWearables: 0,
      numberOfDevices: selectedDevices
    };
    this.handleSmartphoneChange = this.handleSmartphoneChange.bind(this);
    this.handleTabletChange = this.handleTabletChange.bind(this);
    this.handleWearableChange = this.handleWearableChange.bind(this);
  }
  handleSmartphoneChange(changeEvent) {
    const numberOfSmartphones = parseInt(changeEvent.target.value);
    const numberOfTablets = this.state.selectedTablets;
    const numberOfWearables = this.state.selectedWearables;
    this.setState({
      selectedSmartphones: numberOfSmartphones,
      enabledTablets: this.props.maximumDevices - numberOfSmartphones - numberOfWearables,
      enabledWearables: this.props.maximumDevices - numberOfSmartphones - numberOfTablets,
      numberOfDevices: numberOfSmartphones + numberOfTablets + numberOfWearables
    });
  }
  handleTabletChange(changeEvent) {
    const numberOfSmartphones = this.state.selectedSmartphones;
    const numberOfTablets = parseInt(changeEvent.target.value);
    const numberOfWearables = this.state.selectedWearables;
    this.setState({
      enabledSmartphones: this.props.maximumDevices - numberOfTablets - numberOfWearables,
      selectedTablets: numberOfTablets,
      enabledWearables: this.props.maximumDevices - numberOfSmartphones - numberOfTablets,
      numberOfDevices: numberOfSmartphones + numberOfTablets + numberOfWearables
    });
  }
  handleWearableChange(changeEvent) {
    const numberOfSmartphones = this.state.selectedSmartphones;
    const numberOfTablets = this.state.selectedTablets;
    const numberOfWearables = parseInt(changeEvent.target.value);
    this.setState({
      enabledSmartphones: this.props.maximumDevices - numberOfTablets - numberOfWearables,
      enabledTablets: this.props.maximumDevices - numberOfSmartphones - numberOfWearables,
      selectedWearables: numberOfWearables,
      numberOfDevices: numberOfSmartphones + numberOfTablets + numberOfWearables
    });
    this.setState({
      selectedWearables: parseInt(changeEvent.target.value)
    });
  }
  render() {
    const smartphones = Array(this.state.selectedSmartphones).fill("smartphone", 0, this.state.selectedSmartphones);
    const tablets = Array(this.state.selectedTablets).fill("tablet", 0, this.state.selectedTablets);
    const wearables = Array(this.state.selectedWearables).fill("wearable", 0, this.state.selectedWearables);
    const deviceTypes = [...smartphones, ...tablets, ...wearables];
    const planNames = mobilityPlans.getAll().map(
      (plan, index) =>
        <td key={`name${index}`}>
          {plan.name}
        </td>
    );
    const pricesPerLine = mobilityPlans.getAll().map(
      (plan, index) =>
        <td key={`averageCost${index}`}>
          {plan.calculateAverageMonthlyCostPerLine(deviceTypes)}
        </td>
    );
    const totalMonthlyPrices = mobilityPlans.getAll().map(
      (plan, index) =>
        <td key={`totalCost${index}`}>
          {plan.calculateTotalMonthlyCost(deviceTypes)}
        </td>
    );
    const dataFeatures = mobilityPlans.getAll().map(
      (plan, index) =>
        <td key={`data${index}`}>
          <DataFeature plan={plan} />
        </td>
    );
    const nationwideTalkAndTextFeatures = mobilityPlans.getAll().map(
      (_plan, index) =>
        <td key={`nationwideTalkAndText${index}`}>
          Unlimited
        </td>
    );
    const canadaAndMexicoTalkAndTextFeatures = mobilityPlans.getAll().map(
      (_plan, index) =>
        <td key={`canadaAndMexicoTalkAndText${index}`}>
          Unlimited
        </td>
    );
    const streamingFeatures = mobilityPlans.getAll().map(
      (plan, index) =>
        <td key={`streaming${index}`}>
          <StreamingFeature plan={plan} />
        </td>
    );
    const hotspotFeatures = mobilityPlans.getAll().map(
      (plan, index) =>
        <td key={`hotspot${index}`}>
          <HotspotFeature plan={plan} />
        </td>
    );
    const premiumEntertainmentFeatures = mobilityPlans.getAll().map(
      (plan, index) =>
        <td key={`premiumEntertainment${index}`}>
          <PremiumEntertainmentFeature plan={plan} />
        </td>
    );
    return (
      <form>
        <p>{this.state.numberOfDevices} of {this.props.maximumDevices} devices selected</p>
        <Blade type="smartphone">
          <h2>Smartphones</h2>
          <DeviceOptionList
            type="smartphone"
            maximum={this.props.maximumDevices}
            enabled={this.state.enabledSmartphones}
            selected={this.state.selectedSmartphones}
            onChange={this.handleSmartphoneChange}
          />
        </Blade>
        <Blade type="other-devices">
          <h2>Tablets</h2>
          <DeviceOptionList
            type="tablet"
            maximum={this.props.maximumDevices - this.props.minimumDevices}
            enabled={this.state.enabledTablets}
            selected={this.state.selectedTablets}
            onChange={this.handleTabletChange}
          />
          <h2>Wearables</h2>
          <DeviceOptionList
            type="wearable"
            maximum={this.props.maximumDevices - this.props.minimumDevices}
            enabled={this.state.enabledWearables}
            selected={this.state.selectedWearables}
            onChange={this.handleWearableChange}
          />
        </Blade>
        <Blade type="plan">
          <h2>Plans</h2>
          <table>
            <thead>
              <tr>
                <td></td>
                {planNames}
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>Price Per Line</th>
                {pricesPerLine}
              </tr>
              <tr>
                <th>Total Monthly Price for {deviceTypes.length} line{deviceTypes.length > 1 ? "s" : ""}</th>
                {totalMonthlyPrices}
              </tr>
              <tr>
                <th>Data</th>
                {dataFeatures}
              </tr>
              <tr>
                <th>Nationwide Talk & Text</th>
                {nationwideTalkAndTextFeatures}
              </tr>
              <tr>
                <th>Canada & Mexico Talk & Text</th>
                {canadaAndMexicoTalkAndTextFeatures}
              </tr>
              <tr>
                <th>Streaming</th>
                {streamingFeatures}
              </tr>
              <tr>
                <th>Hotspot</th>
                {hotspotFeatures}
              </tr>
              <tr>
                <th>Premium Entertainment</th>
                {premiumEntertainmentFeatures}
              </tr>
            </tbody>
          </table>
        </Blade>
      </form>
    );
  }
}

export default Calculator;