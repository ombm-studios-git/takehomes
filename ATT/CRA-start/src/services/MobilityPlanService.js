import mobilityPlans from "../data/mobilityPlans";
import MobilityPlan from "../models/MobilityPlan";

class MobilityPlanService {
  constructor() {
    this.plans = mobilityPlans.map(
      (plan) => new MobilityPlan(plan)
    );
  }
  getAll() {
    return this.plans;
  }
  get(id) {
    return this.plans.find(
      (plan) => plan.id === id
    );
  }
}

// Singleton
export default new MobilityPlanService();