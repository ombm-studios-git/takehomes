import React from "react";

function formatDefinition(videoQuality) {
  return videoQuality >= 720 ? "HD" : "SD";
}

export default class StreamingFeature extends React.Component {
  render() {
    return (
      <span>
        {this.props.plan.videoQuality}p {formatDefinition(this.props.plan.videoQuality)}
        <br />
        (max of 1.5Mbps)
      </span>
    )
  }
}