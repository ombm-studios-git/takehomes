import React from "react";

const includedMessage = "Choose a favorite on us";
const excludedMessage = "No Premium Entertainment";

export default class PremiumEntertainmentFeature extends React.Component {
  render() {
    return (
      <span>
        {this.props.plan.premiumEntertainment ? includedMessage : excludedMessage}
      </span>
    )
  }
}