import React from "react";

function formatData(megabytes) {
  let formatted = "No Data";
  if (megabytes < 0) {
    formatted = "Unlimited";
  }
  else if (megabytes >= 1000) {
    formatted = `${megabytes / 1000}GB`;
  }
  else if (megabytes > 0) {
    formatted = `${megabytes}MB`;
  }
  return formatted;
}

function formatFootnote(megabytes) {
  let footnote = "Speeds may be slower in congested areas";
  if (megabytes < 0) {
    // TODO: This case does not exists today, but we should confirm the text if it ever comes up
    footnote = "No data management"
  }
  if (megabytes >= 1000) {
    footnote = `After ${megabytes / 1000}GB, speeds may be slower in congested areas`;
  }
  else if (megabytes > 0) {
    footnote = `After ${megabytes}MB, speeds may be slower in congested areas`;
  }
  return footnote;
}

export default class DataFeature extends React.Component {
  render() {
    return (
      <span>
        {formatData(this.props.plan.data)} {this.props.plan.dataSpeed}
        <br />
        <small>{formatFootnote(this.props.plan.dataManagement)}</small>
      </span>
    )
  }
}