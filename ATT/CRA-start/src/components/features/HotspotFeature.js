import React from "react";

function formatHotspot(megabytes) {
  let formatted = "No Hotspot";
  if (megabytes < 0) {
    formatted = "Included in Plan Data";
  }
  else if (megabytes >= 1000) {
    formatted = `${megabytes / 1000}GB`;
  }
  else if (megabytes > 0) {
    formatted = `${megabytes}MB`;
  }
  return formatted;
}

export default class HotspotFeature extends React.Component {
  render() {
    return (
      <span>
        {formatHotspot(this.props.plan.hotspot)}
      </span>
    )
  }
}