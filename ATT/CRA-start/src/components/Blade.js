import React from "react";

class Blade extends React.Component {
  render() {
    let className = "blade";
    if (this.props.type) {
      className = `${this.props.type} ${className}`;
    }
    return (
      <section className={className}>
        {this.props.children}
      </section>
    );
  }
}

export default Blade;