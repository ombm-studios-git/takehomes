import React from "react";

class DeviceOption extends React.Component {
  render() {
    const id = `${this.props.type}s-${this.props.value}`;
    const name = `${this.props.type}s`;
    let className = "device-option";
    if (this.props.isSelected) {
      className = "selected " + className;
    }
    return (
      <li className={className}>
        <label className="device-option-label" htmlFor={id}>
          <input
            id={id}
            className="device-option-button"
            type="radio"
            name={name}
            value={this.props.value}
            checked={this.props.selected}
            disabled={this.props.disabled}
            onChange={this.props.onChange}
          />
          <span className="device-option-text">{this.props.value}</span>
        </label>
      </li>
    );
  }
}

class DeviceOptionList extends React.Component {
  render() {
    const options = [];
    for (let index = 1; index <= this.props.maximum; ++index) {
      options.push(
        <DeviceOption
          key={index}
          type={this.props.type}
          value={index}
          selected={parseInt(this.props.selected) === index}
          disabled={index > this.props.enabled}
          onChange={this.props.onChange}
        />
      );
    }
    return (
      <ul className="device-option-list">
        {options}
      </ul>
    );
  }
}

export default DeviceOptionList;