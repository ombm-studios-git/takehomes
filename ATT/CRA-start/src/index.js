import React from "react";
import ReactDOM from "react-dom";
import Calculator from "./Calculator";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(<Calculator minimumDevices="1" maximumDevices="10" />, document.getElementById("calculator"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();