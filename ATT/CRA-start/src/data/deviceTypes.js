export default [
  {
    "id": "smartphone",
    "name": "Smartphone"
  },
  {
    "id": "basicphone",
    "name": "Basic Phone"
  },
  {
    "id": "tablet",
    "name": "Tablet"
  },
  {
    "id": "wearable",
    "name": "Wearable"
  }
];