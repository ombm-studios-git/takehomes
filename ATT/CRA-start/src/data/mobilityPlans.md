# Mobility Plans JSON Structure

## Number of Lines (NOL) Arrays

Several values depend on the number of lines configured for a plan. In those cases, the first index of the array represents the value to be used when the plan has one line configured; the second index: the value when two lines are configured; etc.

If there are more lines configured than the number of indices in the array, the last value is used. For example, if an array contains only two values, the second value is use for two lines as well as three, four, or ten lines.

    // TODO: Not all devices count toward the "number of lines" value. For example, wearables are not usually counted. It seems like smartphones and basic phones are the only devices that count toward number of lines, but we need to confirm this and implement the appropriate logic throughout.

## `id` (string) (required) (unique)

This is a unique identifier. It should be all lowercase and contain only letters and numbers.

## `name` (string) (required)

This is the full name of the plan.

## `planCost` (number|array) (required)

This defines the base plan cost either as a fixed number or an array. (See the note above about NOL arrays.)

This cost is sometimes referred to as the Shared Data Group (SDG).

## `lineCost` (number|array) (required)

This is an object with a cost property for each device type. Each property name must match the `id` of a device type and each value must be a number.

## `autoBillPayDiscount` (number|array)

This holds the amount of the Auto Bill Pay (ABP) discount to be applied when enabled. The value must be a fixed number or an array. (See the note above about NOL arrays.)

## `multilineDiscount` (number|array)

This holds the discount applied to the base plan cost as an array, where the first index gives the discount for one line, the second index gives the discount for two lines, etc.

This is sometimes called an SDG Discount.

## `data` (number) (required)

This is a number representing the data allowance (in megabytes) for this plan. A value of `-1` represents unlimited data.

    // TODO: Should this be required?

## `dataSpeed` (string) (required)

This is a string representing the default network speed available on this plan. (e.g., `"4G LTE"` or `"5G/5G+"`)

    // TODO: Should this be required?

## `dataManagement` (number) (required)

This is a number indicating the number of megabytes after which AT&T may slow data speeds. A value of `0` means data speeds may always be slowed.

    // TODO: Should this be required?

## `hotspot` (number)

This is a number defining the bandwidth (in megabytes) allowed for mobile hotspot use. A value of `0` indicates that mobile hotspot is not allowed for this plan. A negative value will assume all available data in the plan can be used for mobile hotspot.

## `videoQuality` (number) (required)

This is a number representing the maximum vertical resolution of video data. (e.g., `480` or `1080`) Anything `720` and higher is considered high definition (HD), and anything lower is considered standard definition (SD).

    // TODO: Should this be required?