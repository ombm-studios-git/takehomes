export default class MobilityPlan {
  constructor(settings) {
    Object.assign(this, settings);
  }
  calculatePlanCost(deviceTypes) {
    // TODO: What if deviceTypes is empty or not an array?
    let planCost = 0;
    switch (typeof this.planCost) {
      case "number":
        planCost = this.planCost;
        break;
      case "object":
        const index = Math.min(deviceTypes.length, this.planCost.length) - 1;
        planCost = this.planCost[index];
        break;
    }
    return planCost;
  }
  calculateLineCost(deviceTypes) {
    const costs = deviceTypes.map(
      (deviceType) =>
        this.lineCost[deviceType] || 0
    );
    return costs.reduce((total, cost) => total + cost, 0);
  }
  calculateMultilineDiscount(deviceTypes) {
    let multilineDiscount = 0;
    switch (typeof this.multilineDiscount) {
      case "number":
        multilineDiscount = this.multilineDiscount;
        break;
      case "object":
        const index = Math.min(deviceTypes.length, this.multilineDiscount.length) - 1;
        multilineDiscount = this.multilineDiscount[index];
        break;
    }
    return multilineDiscount;
  }
  calculateAverageMonthlyCostPerLine(deviceTypes) {
    // TODO: What if deviceTypes is empty or not an array?
    const totalCost = this.calculateTotalMonthlyCost(deviceTypes);
    return Math.ceil(100 * totalCost / deviceTypes.length) / 100;
  }
  calculateTotalMonthlyCost(deviceTypes) {
    const planCost = this.calculatePlanCost(deviceTypes);
    const lineCost = this.calculateLineCost(deviceTypes);
    const multilineDiscount = this.calculateMultilineDiscount(deviceTypes);
    return planCost + lineCost - multilineDiscount;
  }
}