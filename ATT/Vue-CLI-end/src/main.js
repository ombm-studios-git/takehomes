import Vue from 'vue'
import App from '@/App.vue'
import store from '@/store'

Vue.config.productionTip = false

import 'animate.css'
import '@/assets/styles/site.scss'

store.dispatch('initialize')

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
