import store from "@/store"

export default class MobilityPlan {
    constructor(settings) {
      Object.assign(this, settings);
    }
    calculatePlanCost() {
      // TODO: What if deviceTypes is empty or not an array?
      let planCost = 0;

      switch (typeof this.planCost) {
        case "number":
          planCost = this.planCost;
          break;
        case "object":
          planCost = this.planCost[Math.min(store.getters.deviceCount, this.planCost.length) - 1];
          break;
        default:
          return planCost;
      }
      return planCost;
    }
    calculateLineCost() {
      const deviceTypes = store.getters.deviceTypes
      const costs = deviceTypes.map(deviceType => this.lineCost[deviceType] || 0);
      return costs.reduce((total, cost) => total + cost, 0);
    }
    calculateMultilineDiscount() {
      let multilineDiscount = 0;
      switch (typeof this.multilineDiscount) {
        case "number":
          multilineDiscount = this.multilineDiscount;
          break;
        case "object":
          multilineDiscount = this.multilineDiscount[Math.min(store.getters.deviceCount, this.multilineDiscount.length) - 1];
          break;
        default:
          return multilineDiscount;
      }
      return multilineDiscount;
    }
    calculateAverageMonthlyCostPerLine() {
      const deviceTypes = store.getters.deviceTypes
      // TODO: What if deviceTypes is empty or not an array?
      const totalCost = this.calculateTotalMonthlyCost();
      return Math.ceil((100 * totalCost) / deviceTypes.length) / 100;
    }
    calculateTotalMonthlyCost() {
      const planCost = this.calculatePlanCost();
      const lineCost = this.calculateLineCost();
      const multilineDiscount = this.calculateMultilineDiscount();
      return planCost + lineCost - multilineDiscount;
    }
  }
  