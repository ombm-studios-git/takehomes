import Vue from "vue"
import Vuex from "vuex"
import MobilityPlanService from "../services/MobilityPlanService"

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        activeBladeIndex: 0,

        deviceCount: 0,
        deviceMaxCount: 10,

        phoneCount: 2,
        phoneRemainingCount: 0,

        tabletCount: 0,
        tabletRemainingCount: 0,

        wearableCount: 0,
        wearableRemainingCount: 0,

        comparisons: [],
        deviceTypes: []
    },
    mutations: {
        setPhoneCount (state, count) {
            state.phoneCount = count
        },
        setTabletCount (state, count) {
            state.tabletCount = count
        },
        setWearableCount (state, count) {
            state.wearableCount = count
        },
        setDeviceCount(state){
            state.deviceCount = state.phoneCount + state.tabletCount + state.wearableCount
            state.phoneRemainingCount = state.deviceMaxCount - (state.tabletCount + state.wearableCount)
            state.tabletRemainingCount = state.deviceMaxCount - (state.phoneCount + state.wearableCount)
            state.wearableRemainingCount = state.deviceMaxCount - (state.phoneCount + state.tabletCount)

            const smartphones = Array(state.phoneCount).fill("smartphone", 0, state.phoneCount)
            const tablets = Array(state.tabletCount).fill("tablet", 0, state.tabletCount)
            const wearables = Array(state.wearableCount).fill("wearable", 0, state.wearableCount)
            state.deviceTypes = [...smartphones, ...tablets, ...wearables]    
        },
        addComparison(state, plan){
            state.comparisons.push(plan)
        },
        navigate(state, index) {
            state.activeBladeIndex = index
        }
    },
    actions: {
        setPhoneCount({commit, state}, count) {
            if(state.phoneRemainingCount < count) return
            if (count !== 0 && count === state.phoneCount) {
                count--
            }            
            commit('setPhoneCount', count)
            commit('setDeviceCount')
        },
        setTabletCount({commit, state}, count) {
            if(state.tabletRemainingCount < count) return
            if (count !== 0 && count === state.tabletCount) {
                count--
            }
            commit('setTabletCount', count)
            commit('setDeviceCount')
        },
        setWearableCount({commit, state}, count) {
            if(state.wearableRemainingCount < count) return
            if (count !== 0 && count === state.wearableCount) {
                count--
            }
            commit('setWearableCount', count)
            commit('setDeviceCount')
        },
        navigate({commit}, index){
            commit('navigate', index)
        },
        initialize({commit}) {            
            commit('setDeviceCount')

            //let plan = MobilityPlanService.get('mobileshareplus3gb')
            MobilityPlanService.getAll().forEach(plan => commit('addComparison', plan))
            //commit('addComparison', plan)
        }
    },
    getters: {
        activeBladeIndex: state => state.activeBladeIndex,

        deviceCount: state => state.deviceCount,
        deviceMaxCount: state => state.deviceMaxCount,

        phoneCount: state => state.phoneCount,
        phoneRemainingCount: state => state.phoneRemainingCount,

        tabletCount: state => state.tabletCount,
        tabletRemainingCount: state => state.tabletRemainingCount,
        
        wearableCount: state => state.wearableCount,
        wearableRemainingCount: state => state.wearableRemainingCount,

        comparisons: state => state.comparisons,
        deviceTypes: state => state.deviceTypes
    }
  })

