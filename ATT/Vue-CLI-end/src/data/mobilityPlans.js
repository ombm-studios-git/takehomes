export default [
    {
      id: "mobileshareplus3gb",
      name: "Mobile Share Plus 3GB",
      planCost: [40, 60],
      lineCost: {
        smartphone: 20,
        tablet: 10,
        wearable: 10
      },
      autoBillPayDiscount: [10, 20],
      data: -1,
      dataSpeed: "4G LTE",
      dataManagement: 0,
      hotspot: -1,
      videoQuality: 480
    },
    {
      id: "mobileshareplus9gb",
      name: "Mobile Share Plus 9GB",
      planCost: [50, 80],
      lineCost: {
        smartphone: 20,
        tablet: 10,
        wearable: 10
      },
      autoBillPayDiscount: [10, 20],
      data: -1,
      dataSpeed: "4G LTE",
      dataManagement: 0,
      hotspot: -1,
      videoQuality: 480
    },
    // {
    //   id: "attunlimitedmore",
    //   name: "AT&T Unlimited &More",
    //   planCost: [45, 75],
    //   lineCost: {
    //     smartphone: 35,
    //     tablet: 20,
    //     wearable: 10
    //   },
    //   autoBillPayDiscount: [10, 20],
    //   data: -1,
    //   dataSpeed: "4G LTE",
    //   dataManagement: 0,
    //   hotspot: 0,
    //   videoQuality: 480
    // },
    // {
    //   id: "attunlimitedmorepremium",
    //   name: "AT&T Unlimited &More Premium",
    //   planCost: [55, 100],
    //   lineCost: {
    //     smartphone: 35,
    //     tablet: 20,
    //     wearable: 10
    //   },
    //   autoBillPayDiscount: [10, 20],
    //   data: -1,
    //   dataSpeed: "4G LTE",
    //   dataManagement: 22000,
    //   hotspot: -1,
    //   videoQuality: 1080,
    //   premiumEntertainment: true
    // },
    {
      id: "attunlimitedstarter",
      name: "AT&T Unlimited Starter",
      planCost: 70,
      lineCost: {
        smartphone: 40,
        tablet: 20,
        wearable: 10
      },
      autoBillPayDiscount: 10,
      multilineDiscount: [35, 10, 25, 50, 70],
      data: -1,
      dataSpeed: "4G LTE",
      dataManagement: 0,
      hotspot: 0,
      videoQuality: 480
    },
    {
      id: "attunlimitedextra",
      name: "AT&T Unlimited Extra",
      planCost: 80,
      lineCost: {
        smartphone: 40,
        tablet: 20,
        wearable: 10
      },
      autoBillPayDiscount: 10,
      multilineDiscount: [35, 0, 15, 40, 60],
      data: -1,
      dataSpeed: "4G LTE",
      dataManagement: 50000,
      hotspot: 15000,
      videoQuality: 480
    },
    {
      id: "attunlimitedmax",
      name: "AT&T Unlimited Max",
      planCost: 100,
      lineCost: {
        smartphone: 40,
        tablet: 20,
        wearable: 10
      },
      autoBillPayDiscount: 10,
      multilineDiscount: [45, 0, 15, 40, 60],
      data: -1,
      dataSpeed: "5G/5G+",
      dataManagement: 100000,
      hotspot: 30000,
      videoQuality: 1080,
      premiumEntertainment: true
    }
  ];
  