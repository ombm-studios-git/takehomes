export default [
    {
      id: "smartphone",
      name: "Smartphone"
    },
    {
      id: "tablet",
      name: "Tablet"
    },
    {
      id: "wearable",
      name: "Wearable"
    }
  ];
  