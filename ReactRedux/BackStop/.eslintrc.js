module.exports = {
	extends: [ 'plugin:react/recommended' ],
	env: {
		es6: true,
		node: true,
		browser: true
	},
	parserOptions: {
		ecmaVersion: 8,
		sourceType: 'module',
		ecmaFeatures: {
			jsx: true
		}
	},
	ignorePatterns: [ '/node_modules/**', '/public/**' ],
	rules: {
		'no-unused-vars': [ 'warn', { args: 'none', argsIgnorePattern: 'req|res|next|val' } ]
	},
	settings: {
		react: {
			version: 'detect'
		}
	}
};
