import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPokemons, pokemonsSelector } from '../redux/slices/pokemons';
import { Container, Row, Col, Button } from 'react-bootstrap';
import PokemonGrid from '../components/PokemonGrid';

import '../styles/App.css';

const App = () => {
	const dispatch = useDispatch();
	const { pokemons, loading, hasErrors } = useSelector(pokemonsSelector);

	useEffect(
		() => {
			dispatch(fetchPokemons());
		},
		[ dispatch ]
	);

	const refreshPage = () => window.location.reload(false);

	const renderPokemons = () => {
		if (loading) return <p>Loading pokemons...</p>;
		if (hasErrors) return <p>Cannot display pokemons...</p>;
		if (!loading && pokemons) {
			return (
				<Row>
					<Col>
						<PokemonGrid pokemons={pokemons} />
					</Col>
				</Row>
			);
		}
	};

	return (
		<section className="App">
			<Container>
				<h1>Pokemons</h1>
				<Button onClick={refreshPage}> Get Random Pokemon </Button>
				{renderPokemons()}
			</Container>
		</section>
	);
};

export default App;
