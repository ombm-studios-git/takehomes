import { createSlice } from '@reduxjs/toolkit';
import { fetchBulbasaur, fetchCharmander, fetchSquirtle, fetchRandomPokemon } from '../../api/index';

export const initialState = {
	loading: false,
	hasErrors: false,
	pokemons: []
};

const pokemonsSlice = createSlice({
	name: 'pokemons',
	initialState,
	reducers: {
		getPokemons: (state) => {
			state.loading = true;
		},
		getPokemonsSuccess: (state, { payload }) => {
			state.pokemons = payload;
			state.loading = false;
			state.hasErrors = false;
		},
		getPokemonsFailure: (state) => {
			state.loading = false;
			state.hasErrors = true;
		}
	}
});

export const { getPokemons, getPokemonsSuccess, getPokemonsFailure } = pokemonsSlice.actions;
export const pokemonsSelector = (state) => state.pokemons;

export default pokemonsSlice.reducer;

export function fetchPokemons() {
	return async (dispatch) => {
		dispatch(getPokemons());

		try {
			const pokemonsList = [ fetchSquirtle, fetchCharmander, fetchBulbasaur, fetchRandomPokemon ];
			const pokemons = await Promise.all(pokemonsList.map((fetches) => fetches()));

			dispatch(getPokemonsSuccess(pokemons));
			return pokemons;
		} catch (error) {
			dispatch(getPokemonsFailure());
			throw new Error(error);
		}
	};
}
