import React from 'react';
import PropTypes from 'prop-types';

const PokemonCard = ({ pokemon }) => {
	return (
		<div className="col mb-2">
			<div className="card" tabIndex={0}>
				<img src={pokemon.sprites.front_default} className="card-img-top" alt={pokemon.species.name} />
				<div className="card-body">
					<h5 className="card-title text-center">{pokemon.species.name}</h5>
					<p className="card-text text-center">
						<span className="embolden">Height:</span> {pokemon.height}
						<br />
						<span className="embolden">Weight:</span> {pokemon.weight}
						<br />
						<span className="embolden">Experience:</span> {pokemon.base_experience}
					</p>
				</div>
			</div>
		</div>
	);
};

export default PokemonCard;

PokemonCard.propTypes = {
	pokemon: PropTypes.object
};
