import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid/v4';
import PokemonCard from './PokemonCard';

const PokemonGrid = ({ pokemons }) => {
	return (
		<Fragment>
			<div className="row row-cols-1 row-cols-md-3 mt-3">
				{Array.from(pokemons).map((pokemon) => <PokemonCard key={uuid()} pokemon={pokemon} />)}
			</div>
		</Fragment>
	);
};
export default PokemonGrid;

PokemonGrid.propTypes = {
	pokemons: PropTypes.arrayOf(PropTypes.object)
};
