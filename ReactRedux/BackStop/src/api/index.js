const BASE_URL = 'https://pokeapi.co/api/v2/pokemon';

const generateRandomNumber = (min, max) => Math.floor(Math.random() * (max - min)) + min;

export const fetchRandomPokemon = async () => {
	let id = generateRandomNumber(1, 35);
	const randomPokemon = await fetch(`${BASE_URL}/${id}`);
	const json = await randomPokemon.json();
	return json;
};

export const fetchSquirtle = async () => {
	try {
		const squirtle = await fetch(`${BASE_URL}/7`);
		return await squirtle.json();
	} catch (error) {
		throw new Error(error);
	}
};

export const fetchCharmander = async () => {
	try {
		const charmander = await fetch(`${BASE_URL}/1`);
		return await charmander.json();
	} catch (error) {
		throw new Error(error);
	}
};

export const fetchBulbasaur = async () => {
	try {
		const bulbasaur = await fetch(`${BASE_URL}/4`);
		return await bulbasaur.json();
	} catch (error) {
		throw new Error(error);
	}
};
