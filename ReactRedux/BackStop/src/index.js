// index.js
import React, { StrictMode } from 'react';
import { render } from 'react-dom';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import App from './containers/App.jsx';
import rootReducer from './redux/slices/index';
import './styles/index.css';
const store = configureStore({ reducer: rootReducer });

render(
	<StrictMode>
		<Provider store={store}>
			<App />
		</Provider>
	</StrictMode>,
	document.getElementById('root')
);
