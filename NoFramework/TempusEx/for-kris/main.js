const attachZipCodes = (data) => {
  const mainContainer = document.getElementById("zipcodes");
  for (let i = 0; i < 21; i++) {
    let div = document.createElement("div");

    div.innerHTML = `${i}. Zip Codes: ${data[i].postal_code}`;
    mainContainer.appendChild(div);
  }
}

const getZipCodes = async () => {
  try {
    let response = await fetch(`https://txmchallenge-zipcodes.netlify.app/ca.json`);
    let zipCodes = await response.json()

    attachZipCodes(zipCodes);

  } catch (err) {
      console.log(err);
  }
}


getZipCodes();