import { getRouterOutlet } from '../support/app.po';

describe('Acute Application', () => {
	beforeEach(() => cy.visit('/'));

	it('should have a router-outlet: ', () => {
		// Custom command example, see `../support/commands.ts` file
		// cy.login('my-email@something.com', 'myPassword');
		const routerOutlet = getRouterOutlet();
		// Function helper example, see `../support/app.po.ts` file
		cy.get('router-outlet').should('exist');
	});
});
