import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BingMapService } from '../../src/app/services/bing-map.service';
import { HttpClient } from '@angular/common/http';

describe('BingMapService', () => {
	beforeEach(() =>
		TestBed.configureTestingModule({
			imports: [ HttpClientTestingModule ],
			declarations: [],
			providers: [ BingMapService, HttpClient ]
		})
	);

	it('should be created', () => {
		const service: BingMapService = TestBed.inject(BingMapService);
		expect(service).toBeTruthy();
	});
});
