import { render } from '@testing-library/angular';
import { FooterComponent } from '../../src/app/ui/components/footer/footer.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('FooterComponent', () => {
	test('should render footer', async () => {
		const component = await render(FooterComponent, {
			imports: [ RouterTestingModule ]
		});
	});
});
