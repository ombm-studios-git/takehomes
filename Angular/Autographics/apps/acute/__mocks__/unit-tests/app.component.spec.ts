import { render } from '@testing-library/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from '../../src/app/app.component';

describe('AppComponent', () => {
	test('should render the app', async () => {
		const component = await render(AppComponent, {
			imports: [ RouterTestingModule ]
		});
	});
});
