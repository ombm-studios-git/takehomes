import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { render } from '@testing-library/angular';

import { LayoutComponent } from '../../src/app/ui/components/layout/layout.component';

describe('LayoutComponent', () => {
	test('should render the layout container', async () => {
		const component = await render(LayoutComponent, {
			imports: [ RouterTestingModule ],
			schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
		});
	});
});
