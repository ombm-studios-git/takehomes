import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { render } from '@testing-library/angular';

import { MainComponent } from '../../src/app/ui/components/main/main.component';

describe('MainComponent', () => {
	test('should render the main section', async () => {
		const component = await render(MainComponent, {
			imports: [ RouterTestingModule ],
			schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
		});
	});
});
