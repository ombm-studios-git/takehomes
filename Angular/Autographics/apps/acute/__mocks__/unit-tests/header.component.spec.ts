import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { LogoComponent } from '../../src/app/ui/components/header/logo/logo.component';
import { TitleComponent } from '../../src/app/ui/components/header/title/title.component';
import { RouterTestingModule } from '@angular/router/testing';
import { render } from '@testing-library/angular';

describe('HeaderComponent', () => {
	test('should render the logo component', async () => {
		const component = await render(LogoComponent, {
			imports: [ RouterTestingModule ],
			schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
		});
	});
	test('should render the title component', async () => {
		const component = await render(TitleComponent, {
			imports: [ RouterTestingModule ],
			schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
		});
	});
});
