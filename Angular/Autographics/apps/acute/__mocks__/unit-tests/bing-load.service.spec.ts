import { TestBed } from '@angular/core/testing';
import { BingLoadService } from '../../src/app/services/bing-load.service';
import { BingMapService } from '../../src/app/services/bing-map.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('BingLoadService', () => {
	beforeEach(() =>
		TestBed.configureTestingModule({
			imports: [ HttpClientTestingModule ],
			declarations: [],
			providers: [ BingMapService, HttpClient ]
		})
	);

	it('should be created', () => {
		const service: BingLoadService = TestBed.inject(BingLoadService);
		expect(service).toBeTruthy();
	});
});
