export class Test {
	public isInSpace = false;
	public boostersLanded = true;

	public launch() {
		this.isInSpace = true;
		this.boostersLanded = true;
	}
}
