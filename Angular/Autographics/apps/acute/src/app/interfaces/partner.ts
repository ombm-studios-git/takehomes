export interface Partner {
	partner: string;
	latitude: number;
	longitude: number;
}
