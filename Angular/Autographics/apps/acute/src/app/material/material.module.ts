import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';

@NgModule({
	declarations: [],
	imports: [ CommonModule, MatButtonModule, MatIconModule, MatListModule, MatSidenavModule, MatToolbarModule ],
	exports: [ FlexLayoutModule, MatButtonModule, MatIconModule, MatListModule, MatSidenavModule, MatToolbarModule ]
})
export class MaterialModule {}
