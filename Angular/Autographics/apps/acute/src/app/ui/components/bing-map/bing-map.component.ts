// tslint:disable-next-line:no-reference
/// <reference path="../../../../../../../node_modules/bingmaps/types/MicrosoftMaps/Microsoft.Maps.d.ts" />
import {
	AfterViewInit,
	ChangeDetectionStrategy,
	Component,
	ElementRef,
	Input,
	OnDestroy,
	OnInit,
	ViewChild
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BingLoadService } from '../../../services/bing-load.service';
import { BingMapService } from '../../../services/bing-map.service';
import { Partner } from '../../../interfaces/partner';
import { environment } from '../../../../environments/environment.prod';

@Component({
	selector: 'ombmstudios-bing-map',
	templateUrl: './bing-map.component.html',
	styleUrls: [ './bing-map.component.css' ],
	providers: [ BingLoadService ],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class BingMapComponent implements OnInit, AfterViewInit, OnDestroy {
	@Input() height: number;
	@ViewChild('bingMap', { static: true })
	bingMap: ElementRef;

	constructor(private bing: BingMapService, private bingLoader: BingLoadService, private http: HttpClient) {}

	ngOnInit(): void {
		this.getMap();
	}

	ngAfterViewInit(): void {
		this.generateMap();
	}

	private getMap(): void {
		this.bingLoader.init(this.bingMap.nativeElement, {
			credentials: environment.mapKey,
			liteMode: true,
			showDashboard: true,
			showZoomButtons: true,
			disableBirdseye: true,
			disableStreetside: false,
			showLocateMeButton: true,
			enableClickableLogo: false,
			disableScrollWheelZoom: true,
			zoom: 9
		});
	}

	generateMap(): void {
		if (!this.bing.map) {
			this.bing.mapReq = this.bingLoader.isMapLoaded.subscribe((map) => {
				if (map) {
					this.bing.jsonReq = this.bing.getJSON().subscribe((partners: Array<Partner>) => {
						this.bing.partners = partners;
						this.bing.createPins(partners);
					});
				}
			});
		} else {
			this.bing.jsonReq = this.bing.getJSON().subscribe((partners: Array<Partner>) => {
				this.bing.partners = partners;
				this.bing.createPins(partners);
			});
		}
	}

	ngOnDestroy(): void {
		if (this.bing.jsonReq) {
			this.bing.jsonReq.unsubscribe();
		}

		if (this.bing.mapReq) {
			this.bing.mapReq.unsubscribe();
		}
	}
}
