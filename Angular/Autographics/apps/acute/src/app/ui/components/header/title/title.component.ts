import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'ombmstudios-title',
	templateUrl: './title.component.html',
	styleUrls: [ './title.component.css' ]
})
export class TitleComponent implements OnInit {
	title = 'Auto Graphics Partners Map';
	isMobile = false;
	getIsMobile(): boolean {
		const w = window.screen.width;
		const breakpoint = 500;
		if (w < breakpoint) {
			return true;
		} else {
			return false;
		}
	}

	constructor() {}

	ngOnInit(): void {
		this.isMobile = this.getIsMobile();
		window.onresize = () => {
			this.isMobile = this.getIsMobile();
		};
	}
}
