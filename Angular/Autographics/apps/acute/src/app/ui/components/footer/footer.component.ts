import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'ombmstudios-footer',
	templateUrl: './footer.component.html',
	styleUrls: [ './footer.component.css' ]
})
export class FooterComponent implements OnInit {
	//  Footer Props
	public developer = "Che' J. Holloway";
	public date = new Date();
	public year = this.date.getFullYear();

	constructor() {}

	ngOnInit(): void {}
}
