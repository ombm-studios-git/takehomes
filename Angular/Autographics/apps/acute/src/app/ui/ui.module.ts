import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/layout/layout.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MainComponent } from './components/main/main.component';
import { BingMapComponent } from './components/bing-map/bing-map.component';
import { TitleComponent } from './components/header/title/title.component';
import { LogoComponent } from './components/header/logo/logo.component';

@NgModule({
	declarations: [ BingMapComponent, FooterComponent, HeaderComponent, LayoutComponent, MainComponent, TitleComponent, LogoComponent ],
	imports: [ CommonModule ],
	schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class UiModule {}
