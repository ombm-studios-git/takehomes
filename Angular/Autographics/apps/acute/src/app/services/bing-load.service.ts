import { BingMapService } from './bing-map.service';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({
	providedIn: 'root'
})
export class BingLoadService {
	private _loadBingService: Promise<void>;
	private _isMapLoaded: Subject<boolean> = new Subject<boolean>();

	public get isMapLoaded(): Subject<boolean> {
		return this._isMapLoaded;
	}

	public set isMapLoaded(value: Subject<boolean>) {
		this._isMapLoaded = value;
	}

	constructor(private bing: BingMapService) {}

	init(element: HTMLElement, options: Microsoft.Maps.IMapLoadOptions): void {
		const MAX_ATTEMPTS = environment.MAX_ATTEMPTS;
		const LAT = environment.LAT;
		const LNG = environment.LNG;
		let attempts = 0;

		this.load()
			.then(() => {
				this.bing.map = new Microsoft.Maps.Map(element, options);
				this.bing.map.setView({
					mapTypeId: Microsoft.Maps.MapTypeId.aerial,
					center: new Microsoft.Maps.Location(LAT, LNG),
					zoom: 5
				});
				this.setInitialInfobox();
				this.isMapLoaded.next(true);
			})
			.catch(() => {
				attempts++;
				if (attempts < MAX_ATTEMPTS) {
					console.log('Initial map load failed, retrying one more time.');
					this.load().then(() => {
						this.bing.map = new Microsoft.Maps.Map(element, options);
						this.setInitialInfobox();
						this.isMapLoaded.next(true);
					});
				} else {
					throw new Error('Max load attempts achieved. Try refreshing.');
				}
			});
	}

	setInitialInfobox() {
		this.bing.infobox = new Microsoft.Maps.Infobox(this.bing.map.getCenter(), {
			title: 'pushpins',
			showCloseButton: true,
			description: 'description',
			visible: false
		});

		this.bing.infobox.setMap(this.bing.map);
	}

	private load(): Promise<void> {
		if (this._loadBingService) {
			return this._loadBingService;
		}

		const script = document.createElement('script');
		script.type = 'text/javascript';
		script.async = true;
		script.defer = true;

		const mapsCallback = 'getMap';
		script.src = `https://www.bing.com/api/maps/mapcontrol?callback=${mapsCallback}`;

		// tslint:disable-next-line:ban-types
		this._loadBingService = new Promise<void>((resolve: Function, reject: Function) => {
			window[mapsCallback] = () => {
				return resolve();
			};
			script.onerror = (error: Event) => {
				console.error('maps script error' + error);
				throw reject(error);
			};
		});

		document.body.appendChild(script);

		return this._loadBingService;
	}
}
