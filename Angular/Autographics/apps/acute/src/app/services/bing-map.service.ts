import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Pin } from '../interfaces/pin';
import { Partner } from '../interfaces/partner';

@Injectable({
	providedIn: 'root'
})
export class BingMapService {
	private PARTNERS_PATH = 'assets/json/partners.json';
	private ICON_PATH = 'assets/icons/pin.png';
	private NUM_PARTNERS = 21;

	public doPinsExist = false;
	public pinLocations: Microsoft.Maps.Location[] = [];
	public jsonReq: any;
	public mapReq: any;
	public map: Microsoft.Maps.Map;
	public partners: Partner[] = [];
	public pins: Microsoft.Maps.Pushpin[] = [];
	public infobox: Microsoft.Maps.Infobox;

	constructor(private http: HttpClient) {}

	public getJSON(): Observable<any> {
		return this.http.get(this.PARTNERS_PATH);
	}

	createPins(partners: Array<Partner>): void {
		// make sure there aren't already pins on the map
		if (!this.doPinsExist) {
			for (const partner of Object.values(partners)) {
				const coord = new Microsoft.Maps.Location(partner.latitude, partner.longitude);
				let pin = new Microsoft.Maps.Pushpin(coord, null);

				this.pinLocations.push(coord);
				pin = this.setPinInfo(pin, coord, partner);
				this.pins.push(pin);
			}

			this.map.entities.push(this.pins);

			if (this.map.entities.getLength() > 0) {
				this.doPinsExist = true;
			}
		}
	}

	placePins(): void {
		if (!this.doPinsExist) {
			this.createPins(this.partners);
		}
	}

	clearPins(): void {
		if (this.doPinsExist) {
			for (let i = this.map.entities.getLength() - 1; i >= 0; i--) {
				const pin = this.map.entities.get(i);
				if (pin instanceof Microsoft.Maps.Polyline) {
					this.map.entities.removeAt(i);
				}

				if (this.infobox) {
					this.hideInfobox();
					this.infobox.setMap(null);
				}

				if (pin instanceof Microsoft.Maps.Pushpin) {
					this.map.entities.removeAt(i);
					this.pins = [];
				}
			}

			this.doPinsExist = false;
		}
	}

	resetInfobox(): void {
		document.getElementById('custom-infobox').remove();
		this.infobox.setOptions({
			htmlContent: ''
		});
	}

	connectPins(): void {
		if (this.doPinsExist) {
			let coordA: Microsoft.Maps.Location;
			let coordB: Microsoft.Maps.Location;
			// tslint:disable-next-line:forin
			for (const key in this.partners) {
				if (Number(key) === this.NUM_PARTNERS - 1) {
					return;
				}

				const partner = this.partners[key];
				const nextPartner = this.partners[Number(key) + 1];

				if (coordA && coordB) {
					coordA = coordB;
					coordB = new Microsoft.Maps.Location(nextPartner.latitude, nextPartner.longitude);
				} else {
					coordA = new Microsoft.Maps.Location(partner.latitude, partner.longitude);
					coordB = new Microsoft.Maps.Location(nextPartner.latitude, nextPartner.longitude);
				}

				const coords: Array<Microsoft.Maps.Location> = [ coordA, coordB ];

				const line = new Microsoft.Maps.Polyline(coords, {
					strokeColor: 'red',
					strokeThickness: 3,
					strokeDashArray: [ 4, 4 ]
				});

				this.map.entities.push(line);
			}
		}
	}

	zoomOnPin(pin: Pin): void {
		const pinCoord = new Microsoft.Maps.Location(pin.latitude, pin.longitude);

		if (this.doPinsExist) {
			this.map.setView({ bounds: Microsoft.Maps.LocationRect.fromLocations(pinCoord) });
		}
	}

	zoomOnPins(pins: Array<Microsoft.Maps.Location>): void {
		if (this.doPinsExist) {
			this.map.setView({ bounds: Microsoft.Maps.LocationRect.fromLocations(pins) });
		}
	}

	setPinInfo(pin: Microsoft.Maps.Pushpin, coord: Microsoft.Maps.Location, partner: Partner): Microsoft.Maps.Pushpin {
		pin.metadata = {
			title: partner.partner,
			description: `Lat: ${partner.latitude}, Long: ${partner.longitude}`,
			location: coord,
			visible: false
		};

		Microsoft.Maps.Events.addHandler(pin, 'click', (e) => {
			this.pinClicked(e);
		});

		this.infobox.setMap(this.map);

		return pin;
	}

	hideInfobox(): void {
		this.infobox.setOptions({
			visible: false
		});
	}

	pinClicked(e) {
		if (e.target.metadata) {
			this.infobox.setOptions({
				title: e.target.metadata.title,
				description: e.target.metadata.description,
				location: e.target.metadata.location,
				visible: true
			});
		}
	}

	changePinColor(col: string): void {
		for (const pin of this.pins) {
			pin.setOptions({
				color: col
			});
		}
	}

	changePinIcon(): void {
		for (const pin of this.pins) {
			pin.setOptions({
				icon: this.ICON_PATH
			});
		}
	}

	changeInfoboxTemplate(): void {
		// tslint:disable-next-line:max-line-length
		const infoboxTemplate =
			'<div id="custom-infobox" class="custom-infobox"><b class="custom-infobox-title">{title}</b><a class="custom-infobox-description">{description}</a></div>\n';

		this.infobox.setOptions({
			htmlContent: infoboxTemplate.replace('{title}', 'myTitle').replace('{description}', 'myDescription')
		});
	}
}
