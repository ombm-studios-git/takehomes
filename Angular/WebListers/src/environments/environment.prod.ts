export const environment = {
  production: true,
  quiz_user: '218',
  appToken: '756A9D65D4BE',
  getUsersUrl:
    'https://api.weblisters.com/quiz/?action=getusers&quiz_user=218&appToken=756A9D65D4BE',
  getUserByIdUrl:
    'https://api.weblisters.com/quiz/?action=getusers&quiz_user=218&appToken=756A9D65D4BE&id=',
  createUserUrl:
    'https://api.weblisters.com/quiz/?action=adduser&quiz_user=218&appToken=756A9D65D4BE',
  deleteUserUrl:
    'https://api.weblisters.com/quiz/?action=deleteuser&quiz_user=218&appToken=756A9D65D4BE&id=',
  updateUserUrl:
    'https://api.weblisters.com/quiz/?action=updateuser&quiz_user=218&appToken=756A9D65D4BE&id='
};
