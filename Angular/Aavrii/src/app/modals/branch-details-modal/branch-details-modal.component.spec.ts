import { HttpClientModule } from "@angular/common/http";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from "@angular/core/testing";
import { RouterModule } from "@angular/router";
import {
  NgbActiveModal,
  NgbModal,
  NgbModule
} from "@ng-bootstrap/ng-bootstrap";
import { FilterPipe, FilterPipeModule } from "ngx-filter-pipe";
import { LoggerConfig, LoggerModule, NgxLoggerLevel } from "ngx-logger";
import { HalifaxApiService } from "../../service/api.halifax";
import { BranchDetailsModalComponent } from "./branch-details-modal.component";

// Mock class for NgbModalRef
export class MockNgbModalRef {
  result: Promise<any> = new Promise((resolve, reject) => resolve("x"));
}

describe("BranchDetailsModalComponent", () => {
  let component: BranchDetailsModalComponent;
  let fixture: ComponentFixture<BranchDetailsModalComponent>;
  let modalService: NgbModal;
  let activeModalService: NgbActiveModal;
  let filterBy: FilterPipe;

  const mockModalRef: MockNgbModalRef = new MockNgbModalRef();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BranchDetailsModalComponent],
      imports: [
        FilterPipeModule,
        HttpClientTestingModule,
        LoggerModule,
        NgbModule,
        RouterModule.forRoot([])
      ],
      providers: [LoggerConfig, NgbActiveModal]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchDetailsModalComponent);
    component = fixture.componentInstance;
    modalService = TestBed.get(NgbModal);
    activeModalService = TestBed.get(NgbActiveModal);
    filterBy = TestBed.get(NgbModal);
    fixture.detectChanges();
  });

  it("should create branch details component", () => {
    expect(component).toBeTruthy();
  });
});
