import { Location } from "@angular/common";
import { Component, Input, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { LoggerConfig } from 'ngx-logger';
import { first } from "rxjs/operators";
import { IBranch } from "../../interfaces/ibranch.interface";
import { HalifaxApiService } from "../../service/api.halifax";

@Component({
  selector: "app-branch-details-modal",
  templateUrl: "./branch-details-modal.component.html",
  styleUrls: ["./branch-details-modal.component.css"],
  providers: [LoggerConfig, NgbActiveModal, HalifaxApiService]
})
export class BranchDetailsModalComponent implements OnInit {
  @Input() title = "Branch Details";
  @Input() postalAddress;
  @Input() name;
  @Input() phoneNumber;
  @Input() address;
  @Input() city;
  @Input() subDivision;
  @Input() postalCode;
  @Input() country;
  @Input() dummyImage;

  branch;

  constructor(
    public activeModal: NgbActiveModal,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: HalifaxApiService
  ) {}

  ngOnInit() {
    this.getBranch(this.route.snapshot.params.id);
  }

  getBranch(id: any) {
    this.apiService
      .getBranch(id)
      .subscribe((data: any) => (this.branch = data));
  }

  back() {
    this.router.navigate(["branches-list"]);
  }
}
