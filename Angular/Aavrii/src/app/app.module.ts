import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import {
  NgbActiveModal,
  NgbModule,
  NgbPaginationModule
} from "@ng-bootstrap/ng-bootstrap";
import { LoggerModule, NgxLoggerLevel } from "ngx-logger";

import { routing } from "./app.routing";

import { AppComponent } from "./app.component";
import { BranchesListComponent } from "./banks/branches-list/branches-list.component";
import { BranchDetailsModalComponent } from "./modals/branch-details-modal/branch-details-modal.component";
import { HalifaxApiService } from "./service/api.halifax";

import { RouterModule } from "@angular/router";
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";
import { FilterPipeModule } from "ngx-filter-pipe";

@NgModule({
  declarations: [
    AppComponent,
    BranchesListComponent,
    BranchDetailsModalComponent
  ],
  entryComponents: [BranchDetailsModalComponent],
  imports: [
    BrowserModule,
    FilterPipeModule,
    FormsModule,
    HttpClientModule,
    LoggerModule.forRoot({
      serverLoggingUrl: "/api/logs",
      level: NgxLoggerLevel.DEBUG,
      serverLogLevel: NgxLoggerLevel.ERROR
    }),
    Ng4LoadingSpinnerModule.forRoot(),
    NgbModule,
    NgbPaginationModule,
    ReactiveFormsModule,
    RouterModule,
    routing,
    BrowserAnimationsModule
  ],
  exports: [FilterPipeModule],
  providers: [HalifaxApiService],
  bootstrap: [AppComponent]
})
export class AppModule {}
